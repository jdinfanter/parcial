<?php

class Facultad{
    private $id;
    private $nombre;
    private $direccion;
    private $telefono;
    private $FacultadDAO;

    public function getId(){
        return $this -> id;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getDireccion(){
        return $this -> direccion;
    }

    public function getTelefono(){
        return $this -> telefono;
    }

    
    
    public function Facultad($id = "", $nombre = "", $direccion = "", $telefono= ""){
        $this -> idCliente = $id;
        $this -> nombre = $nombre;
        $this -> direccion = $direccion;
        $this -> telefono = $telefono;
        $this -> conexion = new Conexion();
        $this -> FacultadDAO = new FacultadDao($this -> id, $this -> nombre, $this -> direccion, $this -> telefono);
    }
   
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> FacultadDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> FacultadDAO -> consultarTodos());
        $facultades= array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $f = new Facultad($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($facultades, $f);
        }
        $this -> conexion -> cerrar();
        return $facultades;
    }
    
}

?>