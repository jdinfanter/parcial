<?php
$nombre = "";
if(isset($_POST["nombre"])){
    $nombre = $_POST["nombre"];
}
$direccion = "";
if(isset($_POST["direccion"])){
    $direccion = $_POST["direccion"];
}  
$telefono = "";
if(isset($_POST["telefono"])){
    $telefono = $_POST["telefono"];
}    
if(isset($_POST["crear"])){
    $Facultad = new Facultad("", $nombre, $cdireccion, $telefono);
    $Facultad -> insertar();    
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Crear Facultad</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="Presentacion/Facultad/CrearFacultad.php" method="post">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $nombre ?>" required>
						</div>
						<div class="form-group">
							<label>Direccion</label> 
							<input type="text" name="cantidad" class="form-control" min="1" value="<?php echo $direccion?>" required>
						</div>
						<div class="form-group">
							<label>telefono</label> 
							<input type="number" name="precio" class="form-control" min="1" value="<?php echo $telefono ?>" required>
						</div>
						<button type="submit" name="crear" class="btn btn-info">Crear</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>