<?php
$Facultad= new Facultad();
$facultades = $Facultad-> consultarTodos();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Consultar Facultad</h4>
				</div>
				<div class="text-right"><?php echo count($facultades) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Direccion</th>
							<th>Telefono</th>
						</tr>
						<?php 
						$i=1;
						foreach($facultades as $facultadActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $facultadActual -> getNombre() . "</td>";
						    echo "<td>" . $facultadActual -> getDireccion() . "</td>";
						    echo "<td>" . $facultadActual -> gettelefono() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
					
				</div>
            </div>
		</div>
	</div>
</div>